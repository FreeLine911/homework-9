import { useState, useEffect } from "react";
import TodoItem from "./components/TodoItem";
import AddTodo from "./components/AddTodo";
import './App.css';

function App() {
    const [todos, setTodos] = useState([]);


    const handleAddTodoItem = (title) => {
        for(let elem of todos){
            if(elem.title === title){
                alert(`you have already ${title} in your todos`)
                return
            }
        }
        setTodos([...todos,{
            id: todos.length>0?(todos[todos.length-1].id+1):0,
            completed: false,
            title
        }
        ])

    }


    const handleChangeTodoItem = (id, completed) => {
        const todoIndex = todos.findIndex((item) => item.id === id);

        if (todoIndex !== -1) {
            const newTodos = [...todos];
            const todo = newTodos[todoIndex];

            newTodos.splice(todoIndex, 1, { ...todo, completed });

            setTodos(newTodos);
        }
    };

    const handlerDeleteTodoItem = (id) => {
        const newTodos = todos.filter((todo) => todo.id !== id);

        setTodos(newTodos);
    }

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/todos?userId=1")
            .then((response) => response.json())
            .then((data) => {
                setTodos(data)
            });
    }, []);

    return (
        <div className="container">
            <h2>{"TODO List:"}</h2>
            <div style={{ margin: "24px 0 48px" }}>
                <AddTodo onAdd={handleAddTodoItem} />
            </div>
            <ul className="collection">
                {[...todos].reverse().map((todo) => <TodoItem
                    key={todo.id}
                    {...todo}
                    onChange={handleChangeTodoItem}
                    onDelete={handlerDeleteTodoItem}
                />)}
            </ul>
        </div>
    );
}

export default App;
