
const AddTodo = ({ onAdd }) => {


    return (<>
        <input id="add-todo"
               type="text"
               placeholder="addTodo"
               onKeyPress={event => {
                   if (event.key === 'Enter') {

                      onAdd(event.target.value)
                      event.target.value = ''
                   }
               }}/>
        <label htmlFor="add-todo">{"Add New Todo"}</label>
    </>);
}

export default AddTodo;
